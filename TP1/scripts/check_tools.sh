#Vérification des instalations suivantes: git, tmux, vim, htop

if ! which vim > /dev/null; then # La commande "which" va permettre de rechercher si la commande demander est installé ou pas
	echo  "vim n'est pas installer! Installer avec la commande apt-get install vim" # S'il n'est pas installé alors on affiche la phrase précédente
	read
	
else
	echo -e "vim est installé!"
fi

if ! which git >/dev/null; then
	echo "git n'est pas intaller! Installer la commande avec apt-get install git"
else 
	echo  " git est déjà installé!"
fi


if ! which tmux >/dev/null; then
	echo "tmux n'est pas intaller! Installer la commande avec apt<F4>get install tmux"
else 
	echo  " tmux est déjà installé!"
fi

if ! which htop >/dev/null; then
	echo  "htop n'est pas intaller! Installer htop avec la commande apt-get install htop"
else 
	echo "htop est installé"
fi
