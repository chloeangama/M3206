#Vérification de l'installation de ssh et de son bon fonctionnement

#!/bin/bash

if ! which ssh >/dev/null; then #On recherche si le démon ssh est bien installé à l'aide de la commande "which"
	echo "ssh n'est pas installé! Installer ssh à l'aide de la commande suivante: apt-get install ssh" #Si elle n'est pas installé, on donne la commande à saisir pour son installation 

else 
	echo "ssh est installé et fonctionne \c Lancement de ssh: " #Sinon, ssh est bien installé 
	/etc/init.d/ssh start # On lance ssh
	service ssh status #Puis on regarde sous status
fi
