#Collecte les informations de l'historique de commandes

HISTFILE=~/.bash_history

#Activation de la commande history
set -o history
echo "Le top 10 des commandes le plus utilisés" 
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -10 #Cette ligne permet de lister en prenant que la colonne de droite, permet de trier, d'enlever les doublons, de mettre des numéros sur le site ainsi que d'avoir les 10 commandes les plus utilisée

echo "Le top 5 des commandes le plus utilisés"
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -5
