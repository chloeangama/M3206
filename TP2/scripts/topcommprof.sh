#!/bin/bash

#Savoir le nombre de commandes à afficher 
echo "Entrez le top X de commandes"
read top

echo "Voici le top $top"

#Récupération de l'emplacement du fichier my_history
var=$(find / -name "my_history")

#Affichage des top des commandes
cat $var | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c | sort -nr | head -$top

#cat $var | awk '{print $2}' permet de lister en prenant que la colonne de droite
# cut -d ';' -f2 permet d'enlever ce qu'il y a avant le ';' et permet de prendre en compre le 2ème champ 
#uniq -c permet d'enlever les doublons
#sort -nr permet de des numéros sur le site
#head permet d'avoir les plus utilisés
