#!/bin/bash

## Script qui permet d'afficher la date à laquelle la première commande donée en paramètre de mon historique a été lancée.

time=$(cat $2 | awk '{print $2}' | grep "$1" | cut -d ':' -f1 | head -1)

# Affichage du fichier deonné en paramètre
# awk '{prin $2}' va permettre de n'afficher que la deuxième colonne
# grep $1 permet de gardre les lignes qui va contenire la commande donné en paramètre
# cut -d va permettre de couper la chaine de caractère où il y a ":"
# -f1 garde la partie avent le ":"
# head -1 permet lui d'avoir la première commande tapé

date -d @$time
#Affichage de la date
# L'option -d suivi de @ va permettre de convertir le timestamp en date 
