#!/bin/bash

#Répertoire de backup
dirbackup=/tmp/backup

#Où la sauvegarder
dest="/home/tah"

# création de l'archive
backupdate=$(date +%Y-%m-%d)
archive="$backupdate.tah.tar.gz"

#Création du répertoire de backup
/bin/mkdir $dirbackup
mv -v  $archive $dirbackup

#affichage du commencement de la sauvegarde
echo "Sauvegarde de $dirbackup vers $dest/$archive "
date 
echo

#Sauvegarde à l'aide de tar
tar czf $dest/$archive $dirbackup

#Affichage de la fin de la sauvegarde
echo
echo "Sauvegarde terminée"
date 

#Lister les détailles des fichiers
ls -lh $dest
