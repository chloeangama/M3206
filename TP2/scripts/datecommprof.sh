#!/bin/bash

## Script qui permet d'afficher la date à laquelle la première commande donée en paramètre de mon historique a été lancée.

# Création des variables first et last
f="first" 
l="last"

# Vérification de l'entré first
if [ $1=$f ]
then time=$(cat $3 | awk '{print $2}' | grep "$2" | cut -d ':' -f1 | head -1)
fi

#Vérification de l'entré last
if [ $1=$l ]
then time=$(cat $3 | awk '{print $2}' | grep "$2" | cut -d ':' -f1 | sort -r | head -1)
fi

# Affichage du fichier deonné en paramètre
# awk '{prin $2}' va permettre de n'afficher que la deuxième colonne
# grep $1 permet de gardre les lignes qui va contenire la commande donné en paramètre
# cut -d va permettre de couper la chaine de caractère où il y a ":"
# -f1 garde la partie avent le ":"
# sort -r inverse la sortie afin d'afficher la dernier fois
# head -1 permet lui d'avoir la première commande tapé

date -d @$time
#Affichage de la date
# L'option -d suivi de @ va permettre de convertir le timestamp en date 
